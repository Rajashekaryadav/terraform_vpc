provider "aws" {
  profile = "terraform_iam_user"
  region  = "ap-south-1"
}

resource "aws_vpc" "sample_vpc" {
  cidr_block = "10.0.0.0/16"

  tags = {
    Name = "first-example"
  }
}

resource "aws_subnet" "public_subnet" {
  vpc_id            = "${aws_vpc.sample_vpc.id}"
  cidr_block        = "10.0.1.0/24"
  availability_zone = "ap-south-1a"

  tags = {
    Name = "public-subnet"
  }
}

resource "aws_subnet" "private_subnet" {
  vpc_id            = "${aws_vpc.sample_vpc.id}"
  cidr_block        = "10.0.2.0/24"
  availability_zone = "ap-south-1b"

  tags = {
    Name = "private-subnet"
  }
}

resource "aws_internet_gateway" "igw" {
  vpc_id = "${aws_vpc.sample_vpc.id}"

  tags = {
    Name = "internet gateway"
  }
}

resource "aws_route_table" "route-table" {
  vpc_id = "${aws_vpc.sample_vpc.id}"

  route {
    cidr_block = "0.0.0.0/0"
    gateway_id = "${aws_internet_gateway.igw.id}"
  }

  tags = {
    Name = "route-table1"
  }
}

resource "aws_route_table_association" "rt-ass" {
  subnet_id      = "${aws_subnet.public_subnet.id}"
  route_table_id = "${aws_route_table.route-table.id}"
}

resource "aws_security_group" "asg" {
  ingress {
    from_port   = 80
    to_port     = 80
    protocol    = "tcp"
    cidr_blocks = ["0.0.0.0/0"]
  }

  ingress {
    from_port   = 443
    to_port     = 443
    protocol    = "tcp"
    cidr_blocks = ["0.0.0.0/0"]
  }

  ingress {
    from_port   = -1
    to_port     = -1
    protocol    = "icmp"
    cidr_blocks = ["0.0.0.0/0"]
  }

  ingress {
    from_port   = 22
    to_port     = 22
    protocol    = "tcp"
    cidr_blocks = ["0.0.0.0/0"]
  }

  vpc_id = "${aws_vpc.sample_vpc.id}"

  tags = {
    Name = "webserver-sec-grp"
  }
}

resource "aws_instance" "webserver" {
  ami                         = "ami-007d5db58754fa284"
  instance_type               = "t2.micro"
  key_name                    = "aws"
  availability_zone           = "ap-south-1a"
  subnet_id                   = "${aws_subnet.public_subnet.id}"
  vpc_security_group_ids      = ["${aws_security_group.asg.id}"]
  associate_public_ip_address = "true"

  tags = {
    Name = "webserver"
  }
}
